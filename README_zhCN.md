# 简介
    易输出设计为一个能够像Ultra-Edit(R)那样以十六进制、二进制和可显示的ASCII码
形式输出一段内存的简单方案。只包含一个头文件，附带的代码文件可以编译出一个示例
程序。

# 使用
    - 要想打印到屏幕，调用EZOUTPUT_D
    - 调用EZOUTPUT_I可以把结果输出到一段内存

这两个宏的参数一样：
1. 需要输出的内存地址
2. 输出的前缀字符串
3. 输入内存地址
4. 输入内存大小
5. 以字符计数的输出宽度
6. 输出的列数，即，一行输出多少字节
7. 多少个列之后显示一个|字符
8. 选项（整数）
        是    ｜否      ｜功能
        －－－｜－－－－｜－－－－－
        偶数  ｜奇数    ｜压缩空行
        负数  ｜自然数  ｜显示二进制

    - 实际输出宽度会在输出宽度和列数之间自动适配较小者。
    - EZOUTPUT_I宏的第一个参数是字符指针，它由宏分配空间并填写，应该在调用宏
之后进行释放。EZOUTPUT_D宏不使用第一个参数。
调用宏的文件名和代码行会在输出中显示。

# 图

![在SourceForge](https://ezproject.sourceforge.io/ezoutput.png)

# 文件

 - ezoutput.h 此项目的所有内容
 - LICENSE\* 使用证书
 - COPYRIGHT 除证书外的使用声明
 - \*_zhCN\* 简体中文
 - sample.c 使用示例
 - AUTHORS 作者信息
 - 本项目此前在[SourceForge](https://sourceforge.net/projects/ezproject/) 维护，且是[易项目](https://ezproject.sourceforge.io/)的子项目。
