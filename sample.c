#include "ezoutput.h"
#include <stdio.h>

int main(int argc, char **argv) {
  char *op = NULL;
  char buf[500];
  int len = sizeof(buf) / sizeof(char);
  for (int i = 0; i < len; i++) {
    // fill the buffer with letters
    if ((i * 3) > len && (i * 3 / 2) < len)
      buf[i] = 0;
    else
      buf[i] = 'a' + (i % 26);
  }
  for (int j = 1; j < 3; j++) { // to show option as odd and even
    EZOUTPUT_I(op, "mod", buf, len, 80, 16, 5, j);
    printf("%s\n", op);
  }
  return 0;
}
