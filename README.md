# Summary
  EZ Output is designed to be an easy solution to output a segment of
memory in hex, binary and printable ASCII format just as Ultra-Edit(R),
with only one header file. The source code file can be used to build a sample
program.

# Usage
  - To print to screen, call EZOUTPUT_D
  - To get a character buffer, call EZOUTPUT_I

Parameters of them,
1. the buffer address that is to be output
2. module name to prefix to output
3. input buffer's address
4. the size of the input buffer
5. the output width measured in character
6. the output column number, i.e, how many bytes to output in a line
7. the column number after which "|" is shown
8. options (signed integer)
    yes  |  no    |  functionality
    -----|--------|----------------------
    even |  odd   |  compress blank lines
    <0   |  >=0   |  display binary

  - The smaller one of width and column parameters are automatically selected.
  - EZOUTPUT_I macro's first parameter is the character pointer that will
be allocated and filled by macro. The space should be freed after usage.
It is not used in EZOUTPUT_D.

# Screenshots

![on SourceForge](https://ezproject.sourceforge.io/ezoutput.png)

# Files

 - ezoutput.h everything this project is
 - LICENSE\* license file
 - COPYRIGHT copyright claim in addition of license
 - \*_zhCN\* in simplified Chinese
 - sample.c example usage
 - AUTHORS author info
 - This project was maintained on [SourceForge](https://sourceforge.net/projects/ezproject/) and a sub project of [EZ Project](https://ezproject.sourceforge.io/).
